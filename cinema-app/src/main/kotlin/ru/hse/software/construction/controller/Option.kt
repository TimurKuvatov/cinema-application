package ru.hse.software.construction.controller

abstract class Option {
    abstract fun getName(): String
    abstract fun startFunction()
}