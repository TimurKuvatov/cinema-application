package ru.hse.software.construction.model

import kotlinx.serialization.Serializable
import ru.hse.software.construction.service.Validator
import java.io.File

@Serializable
class FileContainer(private val path: String) {


    companion object {
        fun getFormat(path: String): String {
            return File(path).extension
        }
    }

    private fun checkExisting(): Boolean {
        return Validator.validatePath(path)
    }

}