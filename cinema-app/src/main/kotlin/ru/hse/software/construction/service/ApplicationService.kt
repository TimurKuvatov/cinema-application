package ru.hse.software.construction.service

import ru.hse.software.construction.converter.PromptConverter
import ru.hse.software.construction.model.TypeOfPrompt
import ru.hse.software.construction.repository.PasswordRepository
import ru.hse.software.construction.starter.Starter
import ru.hse.software.construction.storage.HashPasswordStorage
import ru.hse.software.construction.user.ConsoleUserController
import ru.hse.software.construction.user.ConsoleUserView
import ru.hse.software.construction.user.UserController

private const val SALT_PATH = "salts.txt"
private const val SIGNATURE_PATH = "signature.txt"

class ApplicationService(fileNames: Triple<String, String, String>) {
    private var starter: Starter
    private var authenticationService: AuthenticationService
    private var mapOfTypes: Map<TypeOfPrompt, String> = mapOf(
        Pair(TypeOfPrompt.FILM_NAME, "Input film name"),
        Pair(TypeOfPrompt.FILM_DIRECTOR, "Input director name"),
        Pair(TypeOfPrompt.FILM_GENRE, "Input genre name"),
        Pair(TypeOfPrompt.FILM_DURATION, "Input film duration"),
        Pair(TypeOfPrompt.SESSION_START_TIME_AND_DATE, "Input the start date and time of the session"),
        Pair(TypeOfPrompt.SESSION_NEW_START_TIME_AND_DATE, "Input the new start date and time of the session"),
        Pair(TypeOfPrompt.SUCCESS_OPERATION, "The operation was completed successfully"),
        Pair(TypeOfPrompt.SUCCESS_DELETE_FILM, "The movie was successfully deleted"),
        Pair(TypeOfPrompt.PREVIOUS_FILM_NAME, "Input current film name"),
        Pair(TypeOfPrompt.NEW_FILM_NAME, "Input new film name"),
        Pair(TypeOfPrompt.NEW_DIRECTOR_NAME, "Input new director name"),
        Pair(TypeOfPrompt.NEW_GENRE, "Input genre name"),
        Pair(TypeOfPrompt.NEW_DURATION, "Input new film duration"),
        Pair(TypeOfPrompt.ROW, "Input row number"),
        Pair(TypeOfPrompt.NUMBER_IN_ROW, "Input the seat number in the row"),
        Pair(TypeOfPrompt.LOGIN, "Input login"),
        Pair(TypeOfPrompt.PASSWORD, "Input password"),
        Pair(TypeOfPrompt.DATE, "Input date"),
        Pair(TypeOfPrompt.SAVE_CHANGES, "Make sure that you have saved the changes"),
        Pair(TypeOfPrompt.CHOSE_OPTION, "Choose option:")
    )
    private var promptConverter: PromptConverter<TypeOfPrompt> = PromptConverter(mapOfTypes)
    private var userController: UserController
    private var userView: ConsoleUserView = ConsoleUserView()
    private var hashService: HashService

    init {
        userController = ConsoleUserController(promptConverter, userView)
        hashService = HashService(HashPasswordStorage(PasswordRepository(SALT_PATH, SIGNATURE_PATH)))
        authenticationService = AuthenticationService(userController, hashService)
        starter = Starter(userController, fileNames)
    }

    fun runApplication() {
        authenticationService.runAuthentication()
        starter.runMenu()
    }
}