package ru.hse.software.construction.controller

import ru.hse.software.construction.model.TypeOfPrompt
import ru.hse.software.construction.service.Menu
import ru.hse.software.construction.user.UserController
import kotlin.system.exitProcess

class ExitProgramOption(
    private val userController: UserController, private val menu: Menu?, private val isNeedSaveQuestion: Boolean
) : Option() {
    private val name = "Exit program"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        if (isNeedSaveQuestion) {
            userController.displayMessage(TypeOfPrompt.SAVE_CHANGES)
            menu?.handleMenu(
                listOf(ExitProgramOption(userController, menu, false), ReturnMainMenuOption())
            )
        } else {
            userController.displayMessage("The program has been successfully completed")
            exitProcess(0)
        }
    }
}