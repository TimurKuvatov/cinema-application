package ru.hse.software.construction.model

import kotlinx.serialization.Serializable
import ru.hse.software.construction.model.Seat
import ru.hse.software.construction.model.Session
import java.time.LocalDateTime

@Serializable
data class Ticket(
    val seat: Seat,
    val dateTimeOfBuying: kotlinx.datetime.LocalDateTime,
    val filmName: String,
    val sessionDateTime: kotlinx.datetime.LocalDateTime
)