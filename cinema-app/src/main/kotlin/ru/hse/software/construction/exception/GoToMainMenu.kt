package ru.hse.software.construction.exception

import java.lang.RuntimeException

class GoToMainMenu : RuntimeException()