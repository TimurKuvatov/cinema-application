package ru.hse.software.construction.service

import ru.hse.software.construction.exception.DeleteFilmException
import ru.hse.software.construction.exception.ExceptionStatus
import ru.hse.software.construction.exception.NotFoundException
import ru.hse.software.construction.repository.CinemaSerializer
import ru.hse.software.construction.storage.FilmManager
import ru.hse.software.construction.storage.SessionSchedule
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDate
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import kotlinx.serialization.SerialName
import kotlinx.serialization.json.Json
import ru.hse.software.construction.model.*

class CinemaService(
    private var filmManager: FilmManager,
    private var sessionsSchedule: SessionSchedule,
    private var seatsContainerFactory: SeatsContainerFactory,
    private val pathToFilmFile: String,
    private val pathToScheduleFile: String,
    private val pathToTicketsFile: String,
    private var jsonFormat: Json
) {
    @SerialName("Tickets")
    private var tickets = mutableMapOf<Session, MutableList<Ticket>>()
    private var cinemaSerializer: CinemaSerializer =
        CinemaSerializer(
            this,
            filmManager,
            sessionsSchedule,
            tickets,
            pathToFilmFile,
            pathToScheduleFile,
            pathToTicketsFile,
            jsonFormat
        )

    init {
        deserializeCinema()
    }

    private fun reloadTickets() {
        val sessions = tickets.keys.toList()
        for (i in sessions.indices) {
            val actualSession = sessionsSchedule.getSession(
                sessions[i].film.name,
                sessions[i].startDateTime
            )
            if (actualSession == null) {
                throw NotFoundException(sessions[i].toString())
            } else {
                tickets[sessions[i]]?.let { tickets.put(actualSession, it) }
                tickets.remove(sessions[i])
            }
        }

    }

    fun reloadCinema(
        filmManager: FilmManager,
        sessionsSchedule: SessionSchedule,
        tickets: MutableMap<Session, MutableList<Ticket>>
    ) {
        this.filmManager = filmManager
        this.sessionsSchedule = sessionsSchedule
        this.sessionsSchedule.reloadSessionSchedule(this.filmManager)
        this.tickets = tickets
        reloadTickets()
        cinemaSerializer = CinemaSerializer(
            this,
            filmManager,
            sessionsSchedule,
            tickets,
            pathToFilmFile,
            pathToScheduleFile,
            pathToTicketsFile,
            jsonFormat
        )
    }

    fun addFilm(name: String, director: String, genre: String, duration: kotlin.time.Duration): Boolean {
        return filmManager.addFilm(name, director, genre, duration)
    }

    fun deleteFilm(name: String): Boolean {
        if (sessionsSchedule.checkExistenceSessionWithFilm(name)) {
            throw DeleteFilmException(name)
        }
        return filmManager.deleteFilm(name)
    }

    fun changeFilmName(name: String, newName: String): Boolean {
        return filmManager.changeFilmName(name, newName)
    }

    fun changeFilmDirector(name: String, newDirector: String): Boolean {
        return filmManager.changeFilmDirector(name, newDirector)
    }

    fun changeFilmGenre(name: String, newGenre: String): Boolean {
        return filmManager.changeFilmGenre(name, newGenre)
    }

    fun changeFilmDuration(name: String, newDuration: kotlin.time.Duration): Boolean {
        return filmManager.changeFilmDuration(name, newDuration)
    }

    fun searchFilmByName(name: String): Film? {
        return filmManager.getFilmByName(name)
    }

    fun addSession(filmName: String, dateTime: kotlinx.datetime.LocalDateTime): ExceptionStatus {
        try {
            val session = sessionsSchedule.addSession(filmName, dateTime, seatsContainerFactory.create())
                ?: return ExceptionStatus.OVERLAP_EXCEPTION
            tickets[session] = mutableListOf()
            return ExceptionStatus.NO_EXCEPTION
        } catch (exception: NotFoundException) {
            return ExceptionStatus.NOT_FIND_FILM_EXCEPTION
        }
    }

    fun cancelSession(filmName: String, dateTime: kotlinx.datetime.LocalDateTime): Boolean {
        if (sessionsSchedule.cancelSession(filmName, dateTime)) {
            val session =
                tickets.keys.first { session -> session.film.name == filmName && session.startDateTime == dateTime }
            tickets.remove(session)
            return true
        }
        return false
    }

    fun changeDateTimeOfSession(
        filmName: String,
        dateTime: kotlinx.datetime.LocalDateTime,
        newDateTime: kotlinx.datetime.LocalDateTime
    ): Boolean {
        return sessionsSchedule.changeDateTimeOfSession(filmName, dateTime, newDateTime)
    }

    fun getDaySchedule(date: LocalDate): List<Session>? {
        return sessionsSchedule.getDaySchedule(date)
    }

    fun getFreePlaces(filmName: String, dateTime: kotlinx.datetime.LocalDateTime): Triple<List<Seat>?, Int, Int> {
        return sessionsSchedule.getFreeSeats(filmName, dateTime)
    }

    fun getBookedPlaces(
        filmName: String,
        dateTime: kotlinx.datetime.LocalDateTime
    ): Triple<List<Seat>?, Int, Int> {
        return sessionsSchedule.getBookedSeats(filmName, dateTime)
    }

    fun makeSake(
        row: Int,
        numberInRow: Int,
        filmName: String,
        dateTime: kotlinx.datetime.LocalDateTime
    ): Boolean {
        val session = sessionsSchedule.getSession(filmName, dateTime) ?: return false
        if (!session.seatsContainer.bookSeat(row, numberInRow)) {
            return false
        }
        val instant = Clock.System.now()
        tickets[session]?.add(
            Ticket(
                Seat(row, numberInRow, true),
                instant.toLocalDateTime(TimeZone.currentSystemDefault()),
                session.film.name,
                session.startDateTime
            )
        )
        return true
    }

    fun cancelSake(
        row: Int,
        numberInRow: Int,
        filmName: String,
        dateTime: kotlinx.datetime.LocalDateTime
    ): Boolean {
        val sessions =
            tickets.keys.filter { session ->
                tickets[session]?.find { it.seat.row == row && it.seat.numberInRow == numberInRow && it.filmName == filmName && it.sessionDateTime == dateTime } != null
            }
        if (sessions.isEmpty()) {
            return false
        }
        val session = sessions.first()
        val ticket: Ticket =
            tickets[session]?.firstOrNull { ticket: Ticket ->
                ticket.seat.row == row &&
                        ticket.seat.numberInRow == numberInRow && Clock.System.now()
                    .toLocalDateTime(TimeZone.currentSystemDefault()) <= dateTime
            } ?: return false
        tickets[session]?.remove(ticket)
        session.seatsContainer.cancelSeatBooking(row, numberInRow)
        return true
    }

    fun bookSeat(
        row: Int,
        numberInRow: Int,
        filmName: String,
        dateTime: kotlinx.datetime.LocalDateTime
    ): Boolean {
        val session = sessionsSchedule.getSession(filmName, dateTime) ?: return false
        return session.seatsContainer.bookSeat(row, numberInRow)
    }

    fun cancelSeatBooking(
        row: Int,
        numberInRow: Int,
        filmName: String,
        dateTime: kotlinx.datetime.LocalDateTime
    ): Boolean {
        val session = sessionsSchedule.getSession(filmName, dateTime) ?: return false
        return session.seatsContainer.cancelSeatBooking(row, numberInRow)
    }

    fun deserializeCinema(): Boolean {
        return cinemaSerializer.deserializeCinema()
    }

    fun serializeCinema(): Boolean {
        return cinemaSerializer.serializeCinema()
    }

}