package ru.hse.software.construction.model

import ru.hse.software.construction.storage.SeatsContainer
import kotlinx.serialization.Serializable

@Serializable
data class Session(
    var film: Film,
    var startDateTime: kotlinx.datetime.LocalDateTime,
    val seatsContainer: SeatsContainer
) {
    override fun toString(): String {
        return "startDateTime = ${startDateTime.time} film = ${film.name}; duration = ${film.duration}"
    }
}