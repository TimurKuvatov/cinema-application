package ru.hse.software.construction.storage

import ru.hse.software.construction.exception.NotFoundException
import ru.hse.software.construction.model.Seat
import ru.hse.software.construction.model.Session
import ru.hse.software.construction.model.TimeInterval
import kotlinx.datetime.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Transient

@Serializable
@SerialName("Schedule")
class SingleCinemaHallSessionSchedule(@Transient private var filmManager: FilmManager? = null) : SessionSchedule {
    private var schedule = mutableMapOf<LocalDate, MutableList<Session>>()

    private fun checkOverlap(session: Session): Boolean {
        return schedule.contains(session.startDateTime.date) && schedule[session.startDateTime.date]?.any {
            it != session &&
                    TimeInterval(
                        it.startDateTime.toInstant(UtcOffset(0))
                            .toLocalDateTime(TimeZone.currentSystemDefault()),
                        it.startDateTime.toInstant(UtcOffset(0)).plus(session.film.duration)
                            .toLocalDateTime(TimeZone.currentSystemDefault())
                    ).timePeriodOverlap(
                        TimeInterval(
                            session.startDateTime.toInstant(UtcOffset(0))
                                .toLocalDateTime(TimeZone.currentSystemDefault()),
                            session.startDateTime.toInstant(UtcOffset(0)).plus(it.film.duration)
                                .toLocalDateTime(TimeZone.currentSystemDefault())
                        )
                    )
        } ?: false
    }

    override fun getDaySchedule(date: LocalDate): List<Session>? {
        return schedule[date]?.sortedWith(compareBy { it.startDateTime })
    }

    override fun addSession(
        filmName: String,
        dateTime: LocalDateTime,
        seatsContainer: SeatsContainer
    ): Session? {
        val film = filmManager?.getFilmByName(filmName) ?: throw NotFoundException(filmName)
        val session = Session(film, dateTime, seatsContainer)
        if (schedule.containsKey(dateTime.date)) {
            if (!checkOverlap(session)) {
                schedule[dateTime.date]?.add(session)
                return session
            }
            return null
        } else {
            schedule[dateTime.date] = mutableListOf(session)
            return session
        }
    }

    override fun cancelSession(filmName: String, dateTime: LocalDateTime): Boolean {
        return if (schedule.containsKey(dateTime.date)) {
            schedule[dateTime.date]?.removeIf { it.film.name == filmName && it.startDateTime == dateTime }
                ?: false
        } else {
            false
        }
    }

    override fun changeDateTimeOfSession(
        filmName: String,
        dateTime: LocalDateTime,
        newDateTime: LocalDateTime
    ): Boolean {
        if (schedule.containsKey(dateTime.date)) {
            val session =
                schedule[dateTime.date]?.find { session -> session.film.name == filmName } ?: return false
            if (!checkOverlap(session)) {
                session.startDateTime = newDateTime
                return true
            }
            return false
        }
        return false
    }

    override fun getFreeSeats(filmName: String, dateTime: LocalDateTime): Triple<List<Seat>?, Int, Int> {
        val session =
            schedule[dateTime.date]?.find { session -> session.film.name == filmName && session.startDateTime == dateTime }
                ?: return Triple(null, 0, 0)
        return Triple(
            session.seatsContainer.getFreeSeats(),
            session.seatsContainer.maxNumberOfRows,
            session.seatsContainer.maxNumberOfSeatsInRow
        )
    }

    override fun getBookedSeats(filmName: String, dateTime: LocalDateTime): Triple<List<Seat>?, Int, Int> {
        val session =
            schedule[dateTime.date]?.find { session -> session.film.name == filmName } ?: return Triple(null, 0, 0)
        return Triple(
            session.seatsContainer.getBookedSeats(),
            session.seatsContainer.maxNumberOfRows,
            session.seatsContainer.maxNumberOfSeatsInRow
        )
    }

    override fun reloadSessionSchedule(filmManager: FilmManager) {
        this.filmManager = filmManager
        schedule.forEach { (_, value) ->
            value.forEach { session: Session ->
                val film = filmManager.getFilmByName(session.film.name); if (film == null) throw NotFoundException(
                session.film.name
            ) else session.film = film
            }
        }
    }

    override fun getSession(filmName: String, dateTime: LocalDateTime): Session? {
        return schedule[dateTime.date]?.find { it.startDateTime == dateTime && it.film.name == filmName }
    }

    override fun checkExistenceSessionWithFilm(filmName: String): Boolean {
        return schedule.keys.find { schedule[it]?.find { session -> session.film.name == filmName } != null } != null
    }

}
