package ru.hse.software.construction.storage

import kotlinx.serialization.SerialName
import ru.hse.software.construction.model.Film
import kotlinx.serialization.Serializable


@Serializable
@SerialName("Films")
class FilmManager {
    private var films = mutableListOf<Film>()

    constructor()

    fun addFilm(name: String, director: String, genre: String, duration: kotlin.time.Duration): Boolean {
        return if (films.find { film: Film -> film.name == name } == null) {
            films.add(Film(name, director, genre, duration))
            true
        } else {
            false
        }
    }

    fun deleteFilm(name: String): Boolean {
        return films.remove(films.find { film: Film -> film.name == name })
    }

    fun getFilmByName(name: String): Film? {
        return films.find { film: Film -> film.name == name }
    }

    fun changeFilmName(name: String, newName: String): Boolean {
        val film = getFilmByName(name)
        if (film != null) {
            film.name = newName
            return true
        }
        return false
    }

    fun changeFilmDirector(name: String, newDirector: String): Boolean {
        val film = getFilmByName(name)
        if (film != null) {
            film.director = newDirector
            return true
        }
        return false
    }

    fun changeFilmGenre(name: String, newGenre: String): Boolean {
        val film = getFilmByName(name)
        if (film != null) {
            film.genre = newGenre
            return true
        }
        return false
    }

    fun changeFilmDuration(name: String, newDuration: kotlin.time.Duration): Boolean {
        val film = getFilmByName(name)
        if (film != null) {
            film.duration = newDuration
            return true
        }
        return false
    }
}