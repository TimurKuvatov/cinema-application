package ru.hse.software.construction.service

import ru.hse.software.construction.model.TypeOfPrompt
import ru.hse.software.construction.controller.ExitProgramOption
import ru.hse.software.construction.controller.Option
import ru.hse.software.construction.user.UserController
import ru.hse.software.construction.exception.*


class RegistrationOption(
    private var userController: UserController,
    private var hashService: HashService
) : Option() {
    private val name = "Register"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        val login = userController.readStringAnswer(TypeOfPrompt.LOGIN)
        val password = userController.readStringAnswer(TypeOfPrompt.PASSWORD)
        if (hashService.isAlreadyRegistered(login)) {
            throw AlreadyRegistratedException(login)
        }
        hashService.storageSalt(login)
        hashService.storagePassword(login, password)
        throw GoToMainMenu()
    }
}

class SignOption(
    private var userController: UserController,
    private var hashService: HashService
) : Option() {
    private val name = "Sign in"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        val login = userController.readStringAnswer(TypeOfPrompt.LOGIN)
        val password = userController.readStringAnswer(TypeOfPrompt.PASSWORD)
        if (!hashService.checkPassword(login, password)) {
            throw AuthenticationException()
        }
        throw GoToMainMenu()
    }
}


class AuthenticationService(
    userController: UserController, hashService: HashService
) {
    private var menu: Menu
    private var errorHandler: ErrorHandler

    init {
        errorHandler = ErrorHandler(null, userController)
        menu = Menu(
            userController,
            errorHandler
        )
        menu.reloadOptions(
            listOf(
                RegistrationOption(userController, hashService),
                SignOption(userController, hashService),
                ExitProgramOption(userController, menu, true)
            )
        )
        errorHandler = ErrorHandler(menu, userController)
    }

    fun runAuthentication() {
        while (true) {
            try {
                menu.handleMenu()
                break
            } catch (exception: FileAccessException) {
                errorHandler.handleSignError(exception)
                continue
            } catch (exception: AuthenticationException) {
                errorHandler.handleSignError(exception)
                continue
            } catch (exception: ReturnMainMenu) {
                break
            } catch (exception: AlreadyRegistratedException) {
                errorHandler.handleSignError(exception)
            } catch (exception: GoToMainMenu) {
                break
            } catch (exception: NoLoginException) {
                errorHandler.handleSignError(exception)
            }

        }
    }
}
