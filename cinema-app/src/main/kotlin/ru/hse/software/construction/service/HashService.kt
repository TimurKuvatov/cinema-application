package ru.hse.software.construction.service

import ru.hse.software.construction.exception.NoLoginException
import ru.hse.software.construction.storage.PasswordStorage

class HashService(private val passwordStorage: PasswordStorage) {

    fun checkPassword(login: String, password: String): Boolean {
        try {
            return passwordStorage.checkPassword(login, password)
        } catch (exception: NoLoginException) {
            throw exception
        }
    }

    fun isAlreadyRegistered(login: String): Boolean {
        return passwordStorage.isLoginExists(login)
    }

    fun storageSalt(login: String) {
        return passwordStorage.storageSalt(login)
    }

    fun storagePassword(login: String, password: String) {
        return passwordStorage.storagePassword(login, password)
    }

}