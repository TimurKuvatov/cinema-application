package ru.hse.software.construction.exception

import java.lang.RuntimeException
import java.time.LocalDateTime

class OverlapSessionsException(
    sessionFilmName: String,
    sessionDateTime: kotlinx.datetime.LocalDateTime
) : RuntimeException("The session on $sessionFilmName in $sessionDateTime intersects with another session")