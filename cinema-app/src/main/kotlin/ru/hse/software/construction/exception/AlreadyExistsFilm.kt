package ru.hse.software.construction.exception

import java.lang.RuntimeException

class AlreadyExistsFilm(
    filmName: String
) : RuntimeException("The film with name $filmName already exists")
