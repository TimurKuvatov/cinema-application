package ru.hse.software.construction.controller

import ru.hse.software.construction.service.CinemaService
import ru.hse.software.construction.service.ErrorHandler
import ru.hse.software.construction.user.UserController
import javax.swing.text.DateFormatter


abstract class CinemaOption(
    protected val cinemaService: CinemaService,
    protected val userController: UserController,
    protected val errorHandler: ErrorHandler
) : Option()