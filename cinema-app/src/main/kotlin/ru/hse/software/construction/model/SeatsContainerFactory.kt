package ru.hse.software.construction.model

import ru.hse.software.construction.storage.SeatsContainer
import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable

@Polymorphic
@Serializable
abstract class SeatsContainerFactory {
    abstract fun create(): SeatsContainer
}