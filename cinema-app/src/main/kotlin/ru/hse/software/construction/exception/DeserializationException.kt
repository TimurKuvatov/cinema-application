package ru.hse.software.construction.exception

import java.lang.RuntimeException

class DeserializationException(
    filePath: String
) : RuntimeException("Failed to deserialize from the file $filePath")