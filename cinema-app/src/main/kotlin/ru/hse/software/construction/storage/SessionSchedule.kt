package ru.hse.software.construction.storage

import ru.hse.software.construction.model.Seat
import ru.hse.software.construction.model.Session
import kotlinx.datetime.LocalDate
import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.Polymorphic
import ru.hse.software.construction.storage.SeatsContainer

@Polymorphic
interface SessionSchedule {

    fun addSession(
        filmName: String,
        dateTime: LocalDateTime,
        seatsContainer: SeatsContainer
    ): Session?

    fun cancelSession(filmName: String, dateTime: LocalDateTime): Boolean

    fun getDaySchedule(date: LocalDate): List<Session>?

    fun changeDateTimeOfSession(
        filmName: String,
        dateTime: LocalDateTime,
        newDateTime: LocalDateTime
    ): Boolean

    fun getFreeSeats(filmName: String, dateTime: LocalDateTime): Triple<List<Seat>?, Int, Int>

    fun getBookedSeats(filmName: String, dateTime: LocalDateTime): Triple<List<Seat>?, Int, Int>

    fun reloadSessionSchedule(filmManager: FilmManager)

    fun getSession(filmName: String, dateTime: LocalDateTime): Session?

    fun checkExistenceSessionWithFilm(filmName: String): Boolean
}