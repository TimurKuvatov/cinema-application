package ru.hse.software.construction.exception

import java.lang.RuntimeException

class NoLoginException(
    login: String,
) : RuntimeException("The user with the login $login was not found")