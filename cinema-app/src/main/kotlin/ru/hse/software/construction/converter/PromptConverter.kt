package ru.hse.software.construction.converter

class PromptConverter<TypeOfPrompt>(private val map: Map<TypeOfPrompt, String>) {
    fun convertToPrompt(type: TypeOfPrompt): String? {
        return map[type]
    }
}