package ru.hse.software.construction.controller

import ru.hse.software.construction.exception.ReturnMainMenu

class ReturnMainMenuOption : Option() {
    private val name = "Return to the main menu"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        throw ReturnMainMenu()
    }
}