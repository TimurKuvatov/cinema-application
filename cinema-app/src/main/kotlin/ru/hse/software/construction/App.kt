package ru.hse.software.construction

import ru.hse.software.construction.exception.FileAccessException
import ru.hse.software.construction.exception.IncorrectParamNumberException
import ru.hse.software.construction.service.ApplicationService
import ru.hse.software.construction.service.Validator

val allowedFormats = listOf("json")
fun getFileNames(args: Array<String>): Triple<String, String, String> {
    when (args.size) {
        3 -> {
            val pathToFilmFile = args[0]
            val pathToScheduleFile = args[1]
            val pathToTicketsFile = args[2]
            args.forEach { if (!Validator.validatePath(it, allowedFormats)) throw FileAccessException(it) }
            return Triple(pathToFilmFile, pathToScheduleFile, pathToTicketsFile)
        }

        else -> throw IncorrectParamNumberException(args.size, 3)
    }
}

fun main(args: Array<String>) {
    try {
        val applicationService = ApplicationService(getFileNames(args))
        applicationService.runApplication()
    } catch (exception: IncorrectParamNumberException) {
        println(exception.message)
    } catch (exception: FileAccessException) {
        println(exception.message)
    }

}

