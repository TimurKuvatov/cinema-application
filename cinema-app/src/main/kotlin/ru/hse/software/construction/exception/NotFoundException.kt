package ru.hse.software.construction.exception

import java.lang.RuntimeException

class NotFoundException(
    element: String
) : RuntimeException("$element was not found")