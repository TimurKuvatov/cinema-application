package ru.hse.software.construction.model

import ru.hse.software.construction.storage.RectangularSeatsContainer
import ru.hse.software.construction.storage.SeatsContainer
import kotlinx.serialization.Serializable

@Serializable
class RectangularSeatsFactory(private val numberOfRows: Int, private val numberOfSeatsInRow: Int) :
    SeatsContainerFactory() {

    override fun create(): SeatsContainer {
        return RectangularSeatsContainer(numberOfRows, numberOfSeatsInRow)
    }
}