package ru.hse.software.construction.model

import kotlinx.datetime.LocalDateTime

class TimeInterval(private val startDateTime: LocalDateTime, private val endDateTime: LocalDateTime) {
    fun timePeriodOverlap(otherInterval: TimeInterval): Boolean {
        return otherInterval.startDateTime < endDateTime && otherInterval.endDateTime > startDateTime
    }
}