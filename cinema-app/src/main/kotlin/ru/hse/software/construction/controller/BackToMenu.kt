package ru.hse.software.construction.controller

class BackToMenu : Option() {
    private val name = "Back to menu"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
    }
}