package ru.hse.software.construction.controller

import ru.hse.software.construction.model.Seat
import ru.hse.software.construction.service.CinemaService
import ru.hse.software.construction.model.TypeOfPrompt
import ru.hse.software.construction.service.ErrorHandler
import ru.hse.software.construction.service.Menu
import ru.hse.software.construction.user.UserController
import kotlinx.datetime.toKotlinLocalDate
import kotlinx.datetime.toKotlinLocalDateTime
import ru.hse.software.construction.exception.*
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import kotlin.time.Duration

class CinemaOptions(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
    menu: Menu?,
    dateTimeFormatter: DateTimeFormatter,
    dateFormatter: DateTimeFormatter
) {
    private val cinemaOptions =
        mutableListOf(
            AddFilmOption(cinemaService, userController, errorHandler),
            DeleteFilmOption(cinemaService, userController, errorHandler),
            ChangeFilmOption(cinemaService, userController, errorHandler, menu),
            GetFilmInformation(cinemaService, userController, errorHandler),
            AddSessionOption(cinemaService, userController, errorHandler, dateTimeFormatter),
            CancelSessionOption(cinemaService, userController, errorHandler, dateTimeFormatter),
            ChangeDateTimeOfSessionOption(cinemaService, userController, errorHandler, dateTimeFormatter),
            GetDaySchedule(cinemaService, userController, errorHandler, dateFormatter),
            ShowFreePlaces(cinemaService, userController, errorHandler, dateTimeFormatter),
            MakeSakeOption(cinemaService, userController, errorHandler, dateTimeFormatter),
            CancelSakeOption(cinemaService, userController, errorHandler, dateTimeFormatter),
            BookSeatOption(cinemaService, userController, errorHandler, dateTimeFormatter),
            CancelSeatBooking(cinemaService, userController, errorHandler, dateTimeFormatter),
            SerializeCinema(cinemaService, userController, errorHandler),
            DeserializeCinema(cinemaService, userController, errorHandler)
        )

    fun getCinemaOption(): MutableList<CinemaOption> {
        return cinemaOptions
    }
}

class AddFilmOption(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Add film"
    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        try {
            val filmName = userController.readStringAnswer(TypeOfPrompt.FILM_NAME)
            val duration = Duration.parse(userController.readStringAnswer(TypeOfPrompt.FILM_DURATION))
            if (duration > Duration.parse("20h 30m")) {
                errorHandler.handleError(NotFitBoundariesException("0h", "20h 30m"))
            } else if (cinemaService.addFilm(
                    filmName,
                    userController.readStringAnswer(TypeOfPrompt.FILM_DIRECTOR),
                    userController.readStringAnswer(TypeOfPrompt.FILM_GENRE),
                    duration
                )
            ) {
                userController.displayMessage(TypeOfPrompt.SUCCESS_OPERATION)
            } else {
                errorHandler.handleError(AlreadyExistsFilm(filmName))
            }
        } catch (exception: IllegalArgumentException) {
            errorHandler.handleError(IncorrectFormatException("duration"))
        }
    }
}

class DeleteFilmOption(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Delete film"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        try {
            val filmName = userController.readStringAnswer(TypeOfPrompt.FILM_NAME)
            if (cinemaService.deleteFilm(
                    filmName
                )
            ) {
                userController.displayMessage(TypeOfPrompt.SUCCESS_DELETE_FILM)
            } else {
                errorHandler.handleError(NotFoundException(filmName))
            }
        } catch (exception: DeleteFilmException) {
            errorHandler.handleError(exception)
        }
    }
}

class GetFilmInformation(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Find information about the film"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        val filmName = userController.readStringAnswer(TypeOfPrompt.FILM_NAME)
        val film = cinemaService.searchFilmByName(filmName)
        if (film != null) {
            userController.displayMessage(film.toString())
        } else {
            errorHandler.handleError(NotFoundException(filmName))
        }
    }
}

class ChangeFilmOption(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
    private val menu: Menu?
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Change the data about the film"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        menu?.handleMenu(
            listOf(
                ChangeFilmNameOption(cinemaService, userController, errorHandler),
                ChangeFilmDirector(cinemaService, userController, errorHandler),
                ChangeFilmDuration(cinemaService, userController, errorHandler),
                ChangeFilmGenre(cinemaService, userController, errorHandler)
            )
        )
    }
}

class ChangeFilmNameOption(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Change film name"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        val previousFilmName = userController.readStringAnswer(TypeOfPrompt.PREVIOUS_FILM_NAME)
        val newFilmName = userController.readStringAnswer(TypeOfPrompt.NEW_FILM_NAME)
        if (cinemaService.changeFilmName(
                previousFilmName,
                newFilmName
            )
        ) {
            userController.displayMessage(TypeOfPrompt.SUCCESS_OPERATION)
        } else {
            errorHandler.handleError(NotFoundException(previousFilmName))
        }
    }
}

class ChangeFilmDirector(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Change film director"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        val filmName = userController.readStringAnswer(TypeOfPrompt.FILM_NAME)
        val newDirectorName = userController.readStringAnswer(TypeOfPrompt.NEW_DIRECTOR_NAME)
        if (cinemaService.changeFilmDirector(
                filmName,
                newDirectorName
            )
        ) {
            userController.displayMessage(TypeOfPrompt.SUCCESS_OPERATION)
        } else {
            errorHandler.handleError(NotFoundException(filmName))
        }
    }
}

class ChangeFilmGenre(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Change film genre"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        val filmName = userController.readStringAnswer(TypeOfPrompt.FILM_NAME)
        val newGenre = userController.readStringAnswer(TypeOfPrompt.NEW_GENRE)
        if (cinemaService.changeFilmGenre(
                filmName,
                newGenre
            )
        ) {
            userController.displayMessage(TypeOfPrompt.SUCCESS_OPERATION)
        } else {
            errorHandler.handleError(NotFoundException(filmName))
        }
    }
}

class ChangeFilmDuration(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Change film duration"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        try {
            val filmName = userController.readStringAnswer(TypeOfPrompt.FILM_NAME)
            val newDuration = userController.readStringAnswer(TypeOfPrompt.NEW_DURATION)
            if (cinemaService.changeFilmDuration(
                    filmName,
                    Duration.parse(newDuration)
                )
            ) {
                userController.displayMessage(TypeOfPrompt.SUCCESS_OPERATION)
            } else {
                errorHandler.handleError(NotFoundException(filmName))
            }
        } catch (exception: IllegalArgumentException) {
            errorHandler.handleError(IncorrectFormatException("duration"))
        }
    }
}

class AddSessionOption(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
    private val dateTimeFormatter: DateTimeFormatter
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Add session"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        try {
            val filmName = userController.readStringAnswer(TypeOfPrompt.FILM_NAME)
            val dateTime = LocalDateTime.parse(
                userController.readStringAnswer(TypeOfPrompt.SESSION_START_TIME_AND_DATE),
                dateTimeFormatter
            )
            val status = cinemaService.addSession(
                filmName,
                dateTime.toKotlinLocalDateTime()
            )
            when (status) {
                ExceptionStatus.NO_EXCEPTION -> userController.displayMessage(TypeOfPrompt.SUCCESS_OPERATION)
                ExceptionStatus.OVERLAP_EXCEPTION -> errorHandler.handleError(
                    OverlapSessionsException(
                        filmName,
                        dateTime.toKotlinLocalDateTime()
                    )
                )

                ExceptionStatus.NOT_FIND_FILM_EXCEPTION -> errorHandler.handleError(
                    NotFoundException(
                        filmName
                    )
                )
            }
        } catch (exception: DateTimeParseException) {
            errorHandler.handleError(IncorrectFormatException("dateTime"))
        }
    }
}

class CancelSessionOption(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
    private val dateTimeFormatter: DateTimeFormatter
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Delete session"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        try {
            val filmName = userController.readStringAnswer(TypeOfPrompt.FILM_NAME)
            val dateTime = LocalDateTime.parse(
                userController.readStringAnswer(TypeOfPrompt.SESSION_START_TIME_AND_DATE),
                dateTimeFormatter
            )
            if (cinemaService.cancelSession(
                    filmName,
                    dateTime.toKotlinLocalDateTime()
                )
            ) {
                userController.displayMessage(TypeOfPrompt.SUCCESS_OPERATION)
            } else {
                errorHandler.handleError(
                    NotFoundException("$filmName ${dateTime.toKotlinLocalDateTime()}")
                )
            }
        } catch (exception: DateTimeParseException) {
            errorHandler.handleError(IncorrectFormatException("dateTime"))
        }
    }
}

class ChangeDateTimeOfSessionOption(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
    private val dateTimeFormatter: DateTimeFormatter
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Change session date and time"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        try {
            val filmName = userController.readStringAnswer(TypeOfPrompt.FILM_NAME)
            val previousDateTime = LocalDateTime.parse(
                userController.readStringAnswer(TypeOfPrompt.SESSION_START_TIME_AND_DATE),
                dateTimeFormatter
            )
            val newDateTime = LocalDateTime.parse(
                userController.readStringAnswer(TypeOfPrompt.SESSION_NEW_START_TIME_AND_DATE),
                dateTimeFormatter
            )
            if (cinemaService.changeDateTimeOfSession(
                    filmName,
                    previousDateTime.toKotlinLocalDateTime(),
                    newDateTime.toKotlinLocalDateTime()
                )
            ) {
                userController.displayMessage(TypeOfPrompt.SUCCESS_OPERATION)
            } else {
                errorHandler.handleError(
                    OverlapSessionsException(
                        filmName,
                        newDateTime.toKotlinLocalDateTime()
                    )
                )
            }
        } catch (exception: DateTimeParseException) {
            errorHandler.handleError(IncorrectFormatException("dateTime"))
        }
    }
}

class GetDaySchedule(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
    private val dateTimeFormatter: DateTimeFormatter
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Find out the schedule for the date"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        try {
            val date = LocalDate.parse(
                userController.readStringAnswer(TypeOfPrompt.DATE),
                dateTimeFormatter
            )
            val daySchedule = cinemaService.getDaySchedule(date.toKotlinLocalDate())
            if (daySchedule != null) {
                userController.displayMessage(daySchedule.withIndex()
                    .joinToString(separator = "\n") { pair ->
                        if (pair.index == 0 ||
                            daySchedule[pair.index - 1].startDateTime.date != daySchedule[pair.index].startDateTime.date
                        )
                            "${daySchedule[pair.index].startDateTime.date}\n    ${daySchedule[pair.index]} " else "    ${daySchedule[pair.index]}"
                    })
            } else {
                errorHandler.handleError(
                    NotFoundException("${date.toKotlinLocalDate()}")
                )
            }
        } catch (exception: DateTimeParseException) {
            errorHandler.handleError(IncorrectFormatException("date"))
        }
    }
}

private fun createFreePlacesMessage(freeSeatsData: Triple<List<Seat>?, Int, Int>): String {
    if (freeSeatsData.first != null) {
        val cinemaSeatsState = Array(freeSeatsData.second) { IntArray(freeSeatsData.third) { 1 } }
        freeSeatsData.first!!.forEach { cinemaSeatsState[it.row - 1][it.numberInRow - 1] = 0 }
        val stringMessage = StringBuilder()
        stringMessage.append("Row \\  Seat\t")
        for (i in (1..freeSeatsData.third)) {
            stringMessage.append("$i\t")
        }
        stringMessage.append("\n")
        for (i in 1..freeSeatsData.second) {
            stringMessage.append("$i\t\t")
            for (j in 1..freeSeatsData.third) {
                stringMessage.append(if (cinemaSeatsState[i - 1][j - 1] == 1) "[#]\t" else "[ ]\t")
            }
            stringMessage.append("\n")
        }
        return stringMessage.toString()
    }
    return ""
}

private fun showFreePlaces(
    filmName: String,
    dateTime: LocalDateTime,
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler
): Boolean {
    val freeSeatsData = cinemaService.getFreePlaces(
        filmName,
        dateTime.toKotlinLocalDateTime()
    )
    return if (freeSeatsData.first != null) {
        userController.displayMessage(createFreePlacesMessage(freeSeatsData))
        true
    } else {
        errorHandler.handleError(
            NotFoundException(
                "$filmName ${dateTime.toKotlinLocalDateTime()}"
            )
        )
        false
    }
}


class ShowFreePlaces(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
    private val dateTimeFormatter: DateTimeFormatter
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Display free seats in the session"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        try {
            val filmName = userController.readStringAnswer(TypeOfPrompt.FILM_NAME)
            val dateTime = LocalDateTime.parse(
                userController.readStringAnswer(TypeOfPrompt.SESSION_START_TIME_AND_DATE),
                dateTimeFormatter
            )
            showFreePlaces(filmName, dateTime, cinemaService, userController, errorHandler)
        } catch (exception: DateTimeParseException) {
            errorHandler.handleError(IncorrectFormatException("dateTime"))
        }
    }
}

enum class SakeOperationType {
    MAKE,
    CANCEL
}

private fun sakeFunction(
    sakeOperationType: SakeOperationType,
    userController: UserController,
    dateTimeFormatter: DateTimeFormatter,
    cinemaService: CinemaService,
    errorHandler: ErrorHandler
) {
    val filmName = userController.readStringAnswer(TypeOfPrompt.FILM_NAME)
    val dateTime = LocalDateTime.parse(
        userController.readStringAnswer(TypeOfPrompt.SESSION_START_TIME_AND_DATE),
        dateTimeFormatter
    )
    if (showFreePlaces(filmName, dateTime, cinemaService, userController, errorHandler)) {
        val row = userController.readStringAnswer(TypeOfPrompt.ROW).toInt()
        val numberInRow = userController.readStringAnswer(TypeOfPrompt.NUMBER_IN_ROW).toInt()
        if (if (sakeOperationType == SakeOperationType.MAKE) cinemaService.makeSake(
                row,
                numberInRow,
                filmName,
                dateTime.toKotlinLocalDateTime()
            ) else cinemaService.cancelSake(row, numberInRow, filmName, dateTime.toKotlinLocalDateTime())
        ) {
            userController.displayMessage(TypeOfPrompt.SUCCESS_OPERATION)
            showFreePlaces(filmName, dateTime, cinemaService, userController, errorHandler)
        } else {
            errorHandler.handleError(CheckIfDataCorrect())
        }
    }
}

class MakeSakeOption(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
    private val dateTimeFormatter: DateTimeFormatter
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Record the sale of a ticket for a session"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        try {
            sakeFunction(SakeOperationType.MAKE, userController, dateTimeFormatter, cinemaService, errorHandler)
        } catch (exception: DateTimeParseException) {
            errorHandler.handleError(IncorrectFormatException("dateTime"))
        } catch (exception: NumberFormatException) {
            errorHandler.handleError(IncorrectFormatException("Int"))
        }
    }
}


class CancelSakeOption(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
    private val dateTimeFormatter: DateTimeFormatter
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Refund the ticket before the start of the session"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        try {
            sakeFunction(SakeOperationType.CANCEL, userController, dateTimeFormatter, cinemaService, errorHandler)
        } catch (exception: DateTimeParseException) {
            errorHandler.handleError(CheckIfDataCorrect())
        } catch (exception: NumberFormatException) {
            errorHandler.handleError(IncorrectFormatException("Int"))
        }
    }
}


enum class BookOperationType {
    BOOK,
    CANCEL
}

private fun bookFunction(
    bookOperationType: BookOperationType,
    userController: UserController,
    dateTimeFormatter: DateTimeFormatter,
    cinemaService: CinemaService,
    errorHandler: ErrorHandler
) {
    val filmName = userController.readStringAnswer(TypeOfPrompt.FILM_NAME)
    val dateTime = LocalDateTime.parse(
        userController.readStringAnswer(TypeOfPrompt.SESSION_START_TIME_AND_DATE),
        dateTimeFormatter
    )
    if (showFreePlaces(filmName, dateTime, cinemaService, userController, errorHandler)) {
        val row = userController.readStringAnswer(TypeOfPrompt.ROW).toInt()
        val numberInRow = userController.readStringAnswer(TypeOfPrompt.NUMBER_IN_ROW).toInt()
        if (if (bookOperationType == BookOperationType.BOOK) cinemaService.bookSeat(
                row,
                numberInRow,
                filmName,
                dateTime.toKotlinLocalDateTime()
            ) else cinemaService.cancelSeatBooking(row, numberInRow, filmName, dateTime.toKotlinLocalDateTime())
        ) {
            userController.displayMessage(TypeOfPrompt.SUCCESS_OPERATION)
            showFreePlaces(filmName, dateTime, cinemaService, userController, errorHandler)
        } else {
            errorHandler.handleError(CheckIfDataCorrect())
        }
    }
}

class BookSeatOption(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
    private val dateTimeFormatter: DateTimeFormatter
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Book the seat for the session "

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        try {
            bookFunction(BookOperationType.BOOK, userController, dateTimeFormatter, cinemaService, errorHandler)
        } catch (exception: DateTimeParseException) {
            errorHandler.handleError(IncorrectFormatException("dateTime"))
        } catch (exception: NumberFormatException) {
            errorHandler.handleError(IncorrectFormatException("Int"))
        }
    }
}


class CancelSeatBooking(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
    private val dateTimeFormatter: DateTimeFormatter
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Cancel seat booking for the session "

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        try {
            bookFunction(BookOperationType.CANCEL, userController, dateTimeFormatter, cinemaService, errorHandler)
        } catch (exception: DateTimeParseException) {
            errorHandler.handleError(IncorrectFormatException("dateTime"))
        } catch (exception: NumberFormatException) {
            errorHandler.handleError(IncorrectFormatException("Int"))
        }
    }
}

class SerializeCinema(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Save cinema data to the file"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        if (cinemaService.serializeCinema()) {
            userController.displayMessage("The data has been successfully serialized to the file")
        } else {
            errorHandler.handleError(SerializationException(""))
        }
    }
}


class DeserializeCinema(
    cinemaService: CinemaService,
    userController: UserController,
    errorHandler: ErrorHandler,
) : CinemaOption(cinemaService, userController, errorHandler) {
    private val name = "Restore cinema data from a file"

    override fun getName(): String {
        return name
    }

    override fun startFunction() {
        if (cinemaService.deserializeCinema()) {
            userController.displayMessage("The cinema data was successfully recovered from the file")
        } else {
            errorHandler.handleError(DeserializationException(""))
        }
    }
}
