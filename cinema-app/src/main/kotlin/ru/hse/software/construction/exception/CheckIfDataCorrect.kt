package ru.hse.software.construction.exception

import java.lang.RuntimeException

class CheckIfDataCorrect : RuntimeException("Check the correctness of the entered data")