package ru.hse.software.construction.model

import kotlinx.serialization.Serializable

@Serializable
data class Seat(val row: Int, val numberInRow: Int, var isFree: Boolean) {
    override fun toString(): String {
        return "row = $row; numberInRow = $numberInRow; isFree = $isFree"
    }
}