package ru.hse.software.construction.repository

import ru.hse.software.construction.exception.FileAccessException
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption


class PasswordRepository(private val saltPath: String, private val signaturePath: String) {
    init {
        try {
            val fileSalt = File(saltPath)
            fileSalt.createNewFile()
        } catch (ex: IOException) {
            throw FileAccessException(saltPath)
        }
        try {
            val fileSignature = File(signaturePath)
            fileSignature.createNewFile()
        } catch (ex: IOException) {
            throw FileAccessException(saltPath)
        }
    }

    private fun readMapFromFile(filename: String): MutableMap<String, String> {
        val map = mutableMapOf<String, String>()
        try {
            BufferedReader(FileReader(File(filename))).use { br ->
                var line: String?
                while (br.readLine().also { line = it } != null) {
                    val pair = line?.split(" ")
                    map[pair?.get(0) ?: ""] = pair?.get(1) ?: ""
                }
            }
            return map
        } catch (ex: IOException) {
            throw FileAccessException(filename)
        }
    }

    fun getSalts(): MutableMap<String, String> {
        return readMapFromFile(saltPath)
    }

    fun getHashPasswords(): MutableMap<String, String> {
        return readMapFromFile(signaturePath)
    }

    fun writeSalt(login: String, salt: ByteArray) {
        try {
            Files.write(
                Paths.get(saltPath),
                ("$login ${salt}\n").toByteArray(),
                StandardOpenOption.APPEND,
                StandardOpenOption.CREATE
            )
        } catch (ex: IOException) {
            throw FileAccessException(saltPath)
        }
    }

    fun writePassword(login: String, passwordHash: String) {
        try {
            Files.write(
                Paths.get(signaturePath),
                ("$login ${passwordHash}\n").toByteArray(),
                StandardOpenOption.APPEND,
                StandardOpenOption.CREATE
            )
        } catch (ex: IOException) {
            throw FileAccessException(signaturePath)
        }

    }


}