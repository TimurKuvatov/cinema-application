package ru.hse.software.construction.starter

import ru.hse.software.construction.model.RectangularSeatsFactory
import ru.hse.software.construction.model.SeatsContainerFactory
import ru.hse.software.construction.controller.CinemaOptions
import ru.hse.software.construction.controller.ExitProgramOption
import ru.hse.software.construction.service.CinemaService
import ru.hse.software.construction.service.ErrorHandler
import ru.hse.software.construction.service.Menu
import ru.hse.software.construction.user.UserController
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import ru.hse.software.construction.storage.*
import java.time.format.DateTimeFormatter

const val MAX_ROWS = 10
const val MAX_NUMBER_SEATS_IN_ROW = 20


class Starter(userController: UserController, fileNames: Triple<String, String, String>) {
    private var cinemaService: CinemaService
    private var filmManager: FilmManager = FilmManager()
    private var mapSessionSchedule: SingleCinemaHallSessionSchedule = SingleCinemaHallSessionSchedule(filmManager)
    private var errorHandler: ErrorHandler
    private var menu: Menu
    private var cinemaOptions: CinemaOptions
    private var dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")
    private var dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")
    private var jsonFormat: Json = Json {
        serializersModule = SerializersModule {
            polymorphic(
                SessionSchedule::class,
                SingleCinemaHallSessionSchedule::class,
                SingleCinemaHallSessionSchedule.serializer()
            )
            polymorphic(
                SeatsContainerFactory::class,
                RectangularSeatsFactory::class,
                RectangularSeatsFactory.serializer()
            )
            polymorphic(
                SeatsContainer::class,
                RectangularSeatsContainer::class,
                RectangularSeatsContainer.serializer()
            )
            allowStructuredMapKeys = true
        }
    }

    init {
        cinemaService = CinemaService(
            filmManager,
            mapSessionSchedule,
            RectangularSeatsFactory(MAX_ROWS, MAX_NUMBER_SEATS_IN_ROW),
            fileNames.first,
            fileNames.second,
            fileNames.third,
            jsonFormat
        )
        errorHandler = ErrorHandler(null, userController)
        menu = Menu(
            userController, errorHandler
        )
        cinemaOptions =
            CinemaOptions(cinemaService, userController, errorHandler, menu, dateTimeFormatter, dateFormatter)
        menu.reloadOptions(cinemaOptions.getCinemaOption() + listOf(ExitProgramOption(userController, menu, true)))
        errorHandler = ErrorHandler(menu, userController)
    }

    fun runMenu() {
        menu.handleMenu()
    }
}