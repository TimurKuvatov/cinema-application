package ru.hse.software.construction.service

import ru.hse.software.construction.model.FileContainer
import java.io.File

class Validator {
    companion object {
        fun validateInt(number: Int, minBorder: Int, maxBorder: Int): Boolean {
            return number in minBorder..maxBorder
        }

        fun validatePath(path: String, formats: List<String> = listOf()): Boolean {
            val file = File(path)
            return (formats.contains(FileContainer.getFormat(path)) || formats.isEmpty()) && file.exists() && !file.isDirectory
        }
    }
}