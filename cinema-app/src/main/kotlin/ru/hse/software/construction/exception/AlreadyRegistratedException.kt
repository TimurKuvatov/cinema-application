package ru.hse.software.construction.exception

import java.lang.RuntimeException

class AlreadyRegistratedException(
    login: String
) : RuntimeException("User with login $login already exists")
