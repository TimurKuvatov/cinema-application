package ru.hse.software.construction.exception

enum class ExceptionStatus {
    NO_EXCEPTION,
    NOT_FIND_FILM_EXCEPTION,
    OVERLAP_EXCEPTION
}