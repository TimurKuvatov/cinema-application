package ru.hse.software.construction.storage

import ru.hse.software.construction.exception.FileAccessException
import ru.hse.software.construction.exception.NoLoginException
import ru.hse.software.construction.repository.PasswordRepository
import java.math.BigInteger
import java.security.SecureRandom
import java.security.spec.KeySpec
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

private const val ALGORITHM = "PBKDF2WithHmacSHA512"
private const val ITERATIONS = 120_000
private const val KEY_LENGTH = 256

interface PasswordStorage {
    fun getSalt(login: String): String?
    fun generatePasswordHash(password: String, login: String): String
    fun generateSalt(): ByteArray
    fun storageSalt(login: String)
    fun storagePassword(login: String, password: String) {}
    fun isLoginExists(login: String): Boolean
    fun checkPassword(login: String, password: String): Boolean // can throw NoLoginException()
}

class HashPasswordStorage(private val passwordRepository: PasswordRepository) : PasswordStorage {
    private var salts = passwordRepository.getSalts()
    private var passwordsHashes = passwordRepository.getHashPasswords()
    override fun getSalt(login: String): String? {
        return salts[login]
    }

    override fun generatePasswordHash(password: String, login: String): String {
        val salt = getSalt(login)
        val factory: SecretKeyFactory = SecretKeyFactory.getInstance(ALGORITHM)
        val spec: KeySpec =
            PBEKeySpec(password.toCharArray(), salt?.toByteArray() ?: "".toByteArray(), ITERATIONS, KEY_LENGTH)
        val key: SecretKey = factory.generateSecret(spec)
        val hash: ByteArray = key.encoded
        return toStringHash(hash)
    }

    override fun generateSalt(): ByteArray {
        val sr: SecureRandom = SecureRandom.getInstance("SHA1PRNG")
        val salt = ByteArray(16)
        sr.nextBytes(salt)
        return salt
    }

    override fun storageSalt(login: String) {
        try {
            val salt = generateSalt()
            passwordRepository.writeSalt(login, salt)
            salts[login] = salt.toString()
        } catch (ex: FileAccessException) {
            throw ex
        }
    }


    override fun storagePassword(login: String, password: String) {
        try {
            val passwordHash = generatePasswordHash(password, login)
            passwordRepository.writePassword(login, passwordHash)
            passwordsHashes[login] = passwordHash
        } catch (ex: FileAccessException) {
            throw ex
        }
    }

    private fun toStringHash(array: ByteArray): String {
        val bi = BigInteger(1, array)
        val hex = bi.toString(16)
        val paddingLength = array.size * 2 - hex.length
        return if (paddingLength > 0) {
            String.format("%0" + paddingLength + "d", 0) + hex
        } else {
            hex
        }
    }

    override fun isLoginExists(login: String): Boolean {
        return passwordsHashes.containsKey(login)
    }

    override fun checkPassword(login: String, password: String): Boolean {
        if (!isLoginExists(login)) {
            throw NoLoginException(login)
        }
        val hash = generatePasswordHash(password, login)
        return passwordsHashes[login] == hash
    }
}