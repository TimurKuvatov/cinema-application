package ru.hse.software.construction.exception

import java.lang.RuntimeException

class FileAccessException(
    filePath: String,
) : RuntimeException("The problem of accessing the file on the path $filePath")
