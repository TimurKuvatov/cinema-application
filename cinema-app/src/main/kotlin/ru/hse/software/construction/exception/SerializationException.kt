package ru.hse.software.construction.exception

import java.lang.RuntimeException

class SerializationException(
    filePath: String
) : RuntimeException("Unable to serialize to the file $filePath")