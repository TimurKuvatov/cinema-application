package ru.hse.software.construction.storage

import ru.hse.software.construction.model.Seat
import ru.hse.software.construction.service.Validator
import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable

@Polymorphic
@Serializable
class RectangularSeatsContainer(private val numberOfRows: Int, private val numberOfSeatsInRow: Int) :
    SeatsContainer(numberOfRows, numberOfSeatsInRow) {
    private var seats = Array(numberOfRows) { row ->
        Array(numberOfSeatsInRow) { numberInRow ->
            Seat(
                row + 1,
                numberInRow + 1,
                true
            )
        }
    }


    override fun bookSeat(row: Int, numberInt: Int): Boolean {
        if (Validator.validateInt(row, 1, numberOfRows) && Validator.validateInt(
                numberInt,
                1,
                numberOfSeatsInRow
            ) && seats[row - 1][numberInt - 1].isFree
        ) {
            seats[row - 1][numberInt - 1].isFree = false
            return true
        }
        return false
    }

    override fun cancelSeatBooking(row: Int, numberInt: Int): Boolean {
        if (Validator.validateInt(row, 1, numberOfRows) && Validator.validateInt(
                numberInt,
                1,
                numberOfSeatsInRow
            ) && !seats[row - 1][numberInt - 1].isFree
        ) {
            seats[row - 1][numberInt - 1].isFree = true
            return true
        }
        return false
    }

    private fun getSeatsByFree(isFree: Boolean): List<Seat> {
        return seats.flatMap { it.toList() }.filter { seat -> seat.isFree == isFree }
    }

    override fun getFreeSeats(): List<Seat> {
        return getSeatsByFree(true)
    }

    override fun getBookedSeats(): List<Seat> {
        return getSeatsByFree(false)
    }
}