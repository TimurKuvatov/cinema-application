package ru.hse.software.construction.repository

import ru.hse.software.construction.exception.NotFoundException
import ru.hse.software.construction.service.CinemaService
import ru.hse.software.construction.model.Session
import ru.hse.software.construction.model.Ticket
import ru.hse.software.construction.storage.FilmManager
import ru.hse.software.construction.storage.SessionSchedule
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import java.io.File
import java.io.FileNotFoundException

class CinemaSerializer(
    private val cinemaService: CinemaService,
    private val filmManager: FilmManager,
    private val sessionsSchedule: SessionSchedule,
    private var tickets: MutableMap<Session, MutableList<Ticket>>,
    private val pathToFilmFile: String,
    private val pathToScheduleFile: String,
    private val pathToTicketsFile: String,
    private var jsonFormat: Json
) {
    fun deserializeCinema(): Boolean {
        return try {
            val filmManager = jsonFormat.decodeFromString<FilmManager>(File(pathToFilmFile).readText())
            val sessionsSchedule = jsonFormat.decodeFromString<SessionSchedule>(File(pathToScheduleFile).readText())
            val tickets =
                jsonFormat.decodeFromString<MutableMap<Session, MutableList<Ticket>>>(File(pathToTicketsFile).readText())
            cinemaService.reloadCinema(filmManager, sessionsSchedule, tickets)
            true
        } catch (exception: SerializationException) {
            false
        } catch (exception: FileSystemException) {
            return false
        } catch (exception: NotFoundException) {
            return false
        } catch (exception: FileNotFoundException) {
            return false
        }
    }

    fun serializeCinema(): Boolean {
        return try {
            val stringFilmManager = jsonFormat.encodeToJsonElement(filmManager).toString()
            val stringSessionsSchedule = jsonFormat.encodeToJsonElement(sessionsSchedule).toString()
            val stringTickets = jsonFormat.encodeToJsonElement(tickets).toString()
            File(pathToFilmFile).writeText(stringFilmManager)
            File(pathToScheduleFile).writeText(stringSessionsSchedule)
            File(pathToTicketsFile).writeText(stringTickets)
            true
        } catch (exception: ArithmeticException) {
            false
        } catch (exception: FileSystemException) {
            false
        } catch (exception: SerializationException) {
            false
        }
    }
}