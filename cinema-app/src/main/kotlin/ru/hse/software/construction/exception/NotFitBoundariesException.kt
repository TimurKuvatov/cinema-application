package ru.hse.software.construction.exception

import java.lang.RuntimeException

class NotFitBoundariesException(
    minBoundary: String,
    maxBoundary: String
) : RuntimeException("The input value must be between $minBoundary and $maxBoundary")