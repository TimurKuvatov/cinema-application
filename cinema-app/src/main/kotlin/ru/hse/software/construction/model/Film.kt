package ru.hse.software.construction.model

import kotlinx.serialization.Serializable

@Serializable
data class Film(
    var name: String, var director: String, var genre: String,
    var duration: kotlin.time.Duration
) {
    override fun toString(): String {
        return "name = $name; director = $director; genre = $genre; duration = $duration"
    }
}

