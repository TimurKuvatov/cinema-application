package ru.hse.software.construction.user

import ru.hse.software.construction.model.TypeOfPrompt
import ru.hse.software.construction.converter.PromptConverter

class ConsoleUserController(
    private val promptConverter: PromptConverter<TypeOfPrompt>,
    private val consoleUserView: ConsoleUserView
) : UserController {

    override fun displayMessage(type: TypeOfPrompt) {
        promptConverter.convertToPrompt(type)?.let { consoleUserView.displayMessage(it) }
    }

    override fun displayMessage(message: String) {
        consoleUserView.displayMessage(message)
    }

    override fun readStringAnswer(type: TypeOfPrompt): String {
        displayMessage(type)
        return consoleUserView.readStringAnswer()
    }
}