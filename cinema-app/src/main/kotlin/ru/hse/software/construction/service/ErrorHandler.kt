package ru.hse.software.construction.service

import ru.hse.software.construction.controller.BackToMenu
import ru.hse.software.construction.controller.ExitProgramOption
import ru.hse.software.construction.controller.ReturnMainMenuOption
import ru.hse.software.construction.user.UserController
import java.lang.RuntimeException

class ErrorHandler(private val menu: Menu?, private val userController: UserController) {
    fun handleError(exception: RuntimeException) {
        exception.message?.let { userController.displayMessage(it) }
        menu?.handleMenu(listOf(ExitProgramOption(userController, menu, true), BackToMenu(), ReturnMainMenuOption()))
    }

    fun handleSignError(exception: RuntimeException) {
        exception.message?.let { userController.displayMessage(it) }
        menu?.handleMenu(listOf(ExitProgramOption(userController, menu, true), BackToMenu()))
    }
}