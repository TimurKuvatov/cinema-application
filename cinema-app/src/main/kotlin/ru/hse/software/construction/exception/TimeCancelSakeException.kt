package ru.hse.software.construction.exception

import java.lang.RuntimeException

class TimeCancelSakeException : RuntimeException("Unable to cancel sake after the start of the session")