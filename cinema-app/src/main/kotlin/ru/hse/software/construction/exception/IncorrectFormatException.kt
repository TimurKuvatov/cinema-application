package ru.hse.software.construction.exception

import java.lang.RuntimeException

class IncorrectFormatException(
    format: String,
) : RuntimeException("The input value must be in $format format")