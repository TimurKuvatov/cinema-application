package ru.hse.software.construction.exception

import java.lang.RuntimeException

class AuthenticationException : RuntimeException("Incorrect password")
