package ru.hse.software.construction.exception

import java.lang.RuntimeException


class DeleteFilmException(
    filmName: String
) : RuntimeException("Failed to delete film with name $filmName because of the existence of the session for this film")