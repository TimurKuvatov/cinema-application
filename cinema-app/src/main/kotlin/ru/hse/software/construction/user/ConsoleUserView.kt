package ru.hse.software.construction.user

class ConsoleUserView {

    fun displayMessage(message: String) {
        println(message)
    }

    fun readStringAnswer(): String {
        return readln()
    }
}