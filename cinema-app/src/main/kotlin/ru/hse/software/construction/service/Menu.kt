package ru.hse.software.construction.service

import ru.hse.software.construction.controller.ExitProgramOption
import ru.hse.software.construction.controller.ReturnMainMenuOption
import ru.hse.software.construction.controller.Option
import ru.hse.software.construction.user.UserController
import ru.hse.software.construction.exception.IncorrectFormatException
import ru.hse.software.construction.exception.NotFitBoundariesException
import ru.hse.software.construction.exception.ReturnMainMenu
import ru.hse.software.construction.model.TypeOfPrompt


class Menu(
    private var options: List<Option>,
    private val userController: UserController,
    private val errorHandler: ErrorHandler
) {

    constructor(userController: UserController, errorHandler: ErrorHandler) : this(
        emptyList(),
        userController,
        errorHandler
    )

    fun reloadOptions(options: List<Option>) {
        this.options = options
    }

    private fun choseFunction(minBorder: Int, maxBorder: Int): Int {
        val numberOfFunction: Int
        var inputNumber: Int
        do {
            try {
                inputNumber = userController.readStringAnswer(TypeOfPrompt.CHOSE_OPTION).toInt()
            } catch (ex: NumberFormatException) {
                errorHandler.handleError(IncorrectFormatException("Int"))
                continue
            }
            if (!Validator.validateInt(inputNumber, minBorder, maxBorder)) {
                errorHandler.handleError(
                    NotFitBoundariesException(
                        minBorder.toString(),
                        maxBorder.toString()
                    )
                )
                continue
            }
            numberOfFunction = inputNumber
            break
        } while (true)
        return numberOfFunction
    }

    private fun choseFunction(): Int {
        return choseFunction(0, options.size)
    }

    private fun optionsToString(options: List<Option>): String {
        return "Menu\n" + options.withIndex()
            .joinToString(separator = "\n") { pair -> "${pair.index} ${pair.value.getName()}" }
    }

    override fun toString(): String {
        return optionsToString(options)
    }


    fun handleMenu(optionsForPostMenu: List<Option> = emptyList()) {
        try {
            if (optionsForPostMenu.isEmpty()) {
                userController.displayMessage(this.toString())
                val optionNumber = choseFunction()
                options[optionNumber].startFunction()
                handleMenu(
                    listOf(
                        ExitProgramOption(
                            userController,
                            this, true
                        ), ReturnMainMenuOption()
                    )
                )
            } else {
                userController.displayMessage(optionsToString(optionsForPostMenu))
                val optionNumber = choseFunction(0, optionsForPostMenu.size - 1)
                optionsForPostMenu[optionNumber].startFunction()
            }
        } catch (signal: ReturnMainMenu) {
            handleMenu()
        }
    }
}