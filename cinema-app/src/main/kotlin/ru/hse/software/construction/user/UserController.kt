package ru.hse.software.construction.user

import ru.hse.software.construction.model.TypeOfPrompt

interface UserController {
    fun displayMessage(message: String)
    fun displayMessage(type: TypeOfPrompt)
    fun readStringAnswer(type: TypeOfPrompt): String
}