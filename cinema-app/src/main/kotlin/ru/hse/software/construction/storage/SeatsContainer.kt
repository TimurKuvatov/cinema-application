package ru.hse.software.construction.storage

import ru.hse.software.construction.model.Seat
import kotlinx.serialization.Serializable
import kotlinx.serialization.Polymorphic

@Polymorphic
@Serializable
abstract class SeatsContainer(val maxNumberOfRows: Int, val maxNumberOfSeatsInRow: Int) {
    abstract fun bookSeat(row: Int, numberInt: Int): Boolean
    abstract fun cancelSeatBooking(row: Int, numberInt: Int): Boolean
    abstract fun getFreeSeats(): List<Seat>
    abstract fun getBookedSeats(): List<Seat>
}