package ru.hse.software.construction.exception

import java.lang.RuntimeException

class IncorrectParamNumberException(
    actualParamNumber: Int,
    requiredParamNumber: Int,
) : RuntimeException("Incorrect number of parameters: $actualParamNumber parameters were entered, the required number of parameters is $requiredParamNumber")